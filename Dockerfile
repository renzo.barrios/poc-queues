FROM node:lts-alpine

WORKDIR /var/www

COPY ./dist ./dist
COPY ./package.json ./package.json
COPY ./node_modules ./node_modules

CMD ["npm", "start"]

EXPOSE 3000

USER node