## POC Nestjs Queues

![img](https://1.bp.blogspot.com/-d_BhfQKKpdM/YGh-U0qW4HI/AAAAAAAAq-c/k_mAQVA9V6oRzZxAWlCZPXGzUJ9G1Wc_gCLcBGAsYHQ/s16000/dd.jpg)

#### ¿Cuando usar Queues?

Las colas son un poderoso patrón de diseño que ayuda a resolver los problemas más comunes de escalado y rendimiento de las aplicaciones. Algunos ejemplos de problemas que las colas pueden ayudarle a resolver son

- Suavizar los picos de procesamiento. Por ejemplo, si los usuarios pueden iniciar tareas que consumen muchos recursos en momentos arbitrarios, puede añadir estas tareas a una cola en lugar de realizarlas de forma sincrónica. A continuación, puede hacer que los procesos de los trabajadores(Jobs) extraigan tareas de la cola de forma controlada. Puede añadir fácilmente nuevos consumidores de cola para ampliar la gestión de tareas de back-end a medida que la aplicación se amplía.

- Divida las tareas monolíticas que de otro modo podrían bloquear el bucle de eventos de Node.js. Por ejemplo, si una solicitud del usuario requiere un trabajo intensivo de la CPU como la transcodificación de audio, puede delegar esta tarea a otros procesos, liberando los procesos orientados al usuario para que sigan respondiendo.

- Proporcionar un canal de comunicación fiable entre varios servicios. Por ejemplo, puede poner en cola tareas (jobs) en un proceso o servicio, y consumirlas en otro. Puede recibir notificaciones (mediante la escucha de eventos de estado) en caso de finalización, error u otros cambios de estado en el ciclo de vida del trabajo desde cualquier proceso o servicio. Cuando los productores o consumidores de colas fallan, su estado se conserva y la gestión de las tareas puede reiniciarse automáticamente cuando se reinician los nodos.

**_[Nestjs Queues docs](https://docs.nestjs.com/techniques/queues)_**

#### 1 Añadir Bull(gestor de colas) a app.module

```
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
```

#### 2 Registrar Cola(queues) al modulo a utilizar

```
    BullModule.registerQueue({
      /*name that need to be asociated with the producer*/
      name: 'report-queue',
    }),
```

#### 3 Crear el producer (en cual produce el job y lo agrega a Redis)

```
@Injectable()
export class ReportProducerService {
  // Nombre del la cola especificado en el modulo (key)
  constructor(@InjectQueue('report-queue') private queue: Queue) {}

  async sendReport(report: CreateReportDto) {
    // Añadir job a la cola para luego ser consumida por el producer
    await this.queue.add('report-job', report, { delay: 10000, attempts: 5 });
  }
}
```

#### 4 Utilizar el Consumer que recibe el Job

```
@Processor('report-queue')
export class ReportConsumer {
  // Nombre del job registrado
  @Process('report-job')
  async readOperationJob(job: Job<CreateReportDto>) {
    //file written successfully
    console.log('file written successfully');
  }
}
```
