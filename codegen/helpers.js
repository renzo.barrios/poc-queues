const dashify = require('dashify');
const pluralize = require('pluralize');
const { sentenceCase } = require('sentence-case');

module.exports = {
  sentenceAndPluralCase: text => sentenceCase(pluralize(text)),
  pluralUpperCase: text => pluralize(text).toLowerCase(),
  pluralLowerCase: text => pluralize(text).toLowerCase(),
  dashLowerCase: text => dashify(text).toLowerCase(),
  dashAndPluralLowerCase: text => dashify(pluralize(text)).toLowerCase(),
  dashAndPluralUpperCase: text => dashify(pluralize(text)).toUpperCase(),
};
