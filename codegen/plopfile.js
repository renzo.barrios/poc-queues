/* eslint-disable */
const helpers = require('./helpers');

const crudPrompts = [
  {
    type: 'input',
    name: 'EntityName',
    message: 'What is the Entity name? eg: "User"',
  },
];

const crudActions = [
  {
    type: 'add',
    path: `../src/{{dashAndPluralLowerCase EntityName}}/dto/create-{{dashLowerCase EntityName}}.dto.ts`,
    templateFile: `templates/dto/create.hbs`,
  },
  {
    type: 'add',
    path: `../src/{{dashAndPluralLowerCase EntityName}}/dto/update-{{dashLowerCase EntityName}}.dto.ts`,
    templateFile: `templates/dto/update.hbs`,
  },
  {
    type: 'add',
    path: `../src/{{dashAndPluralLowerCase EntityName}}/entity/{{dashLowerCase EntityName}}.entity.ts`,
    templateFile: `templates/entity/base.hbs`,
  },
  {
    type: 'add',
    path: `../src/{{dashAndPluralLowerCase EntityName}}/{{dashLowerCase EntityName}}.controller.ts`,
    templateFile: `templates/controller/base.hbs`,
  },
  {
    type: 'add',
    path: `../src/{{dashAndPluralLowerCase EntityName}}/{{dashLowerCase EntityName}}.service.ts`,
    templateFile: `templates/service/base.hbs`,
  },
  {
    type: 'add',
    path: `../src/{{dashAndPluralLowerCase EntityName}}/{{dashLowerCase EntityName}}.module.ts`,
    templateFile: `templates/module/base.hbs`,
  },
];

module.exports = plop => {
  Object.entries(helpers).forEach(([helperName, helperFunction]) => {
    plop.setHelper(helperName, helperFunction);
  });

  plop.setGenerator(`crud-codegen`, {
    description: `Creates a CRUD`,
    prompts: crudPrompts,
    actions: crudActions,
  });
};
