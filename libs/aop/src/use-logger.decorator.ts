import { Logger } from '@nestjs/common';

export const UseLogger = () => {
  return (target: any, property: string, descriptor: PropertyDescriptor) => {
    const fn: Function = descriptor.value;

    descriptor.value = async function (...args) {
      try {
        Logger.log(
          `Executing ${property} with params: ${JSON.stringify([...args])}`,
          target.constructor.name,
        );

        const result = await fn.call(this, ...args);

        Logger.log(
          `Finished ${property} execution with result: ${JSON.stringify(
            result,
          )}`,
          target.constructor.name,
        );

        return result;
      } catch (err) {
        Logger.error(
          `Finished ${property} execution with error: ${err.message}`,
          target.constructor.name,
        );

        throw err;
      }
    };

    return descriptor;
  };
};
