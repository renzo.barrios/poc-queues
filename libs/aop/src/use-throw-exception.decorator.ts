import { InternalServerErrorException } from '@nestjs/common';

export const UseThrowException = (
  ExceptionClass = InternalServerErrorException,
) => {
  return (target: any, property: string, descriptor: PropertyDescriptor) => {
    const fn: Function = descriptor.value;

    descriptor.value = async function (...args) {
      try {
        return await fn.call(this, ...args);
      } catch (err) {
        throw new ExceptionClass(err);
      }
    };

    return descriptor;
  };
};
