import { SetMetadata } from '@nestjs/common';

export const NOT_JSON_RESPONSE = 'NotJsonResponse';

export const NotJsonResponse = () => SetMetadata(NOT_JSON_RESPONSE, true);
