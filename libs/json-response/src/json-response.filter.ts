import {
  HttpException,
  HttpStatus,
  ArgumentsHost,
  Catch,
  ExceptionFilter,
} from '@nestjs/common';
import { classToPlain } from 'class-transformer';
import { Request, Response } from 'express';

import { JsonError, JsonMeta, JsonPage, JsonResponse } from './json-response';

@Catch()
export class JsonResponseExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();

    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();

    const meta = new JsonMeta();

    meta.url = request.url;
    meta.method = request.method;

    const error = new JsonError();

    if (exception instanceof HttpException) {
      meta.status = exception.getStatus();

      error.code = exception.name;
      error.detail = exception.message;
    } else {
      meta.status = HttpStatus.INTERNAL_SERVER_ERROR;

      error.code = exception.code || null;
      error.detail = exception.message;
    }

    meta.paging = new JsonPage();

    const json = new JsonResponse();

    json.meta = meta;
    json.data = null;
    json.errors = [error];

    response.status(meta.status).json(classToPlain(json));
  }
}
