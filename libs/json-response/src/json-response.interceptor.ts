import {
  Injectable,
  CallHandler,
  NestInterceptor,
  ExecutionContext,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reflector } from '@nestjs/core';
import { Request, Response } from 'express';
import { classToPlain } from 'class-transformer';

import { NOT_JSON_RESPONSE } from './json-response.decorator';
import { JsonMeta, JsonPage, JsonResponse } from './json-response';

@Injectable()
export class JsonResponseInterceptor<T> implements NestInterceptor<T, any> {
  constructor(private reflector: Reflector) {}

  public intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();

    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();

    const isNotJson = this.reflector.getAllAndOverride<boolean>(
      NOT_JSON_RESPONSE,
      [context.getHandler(), context.getClass()],
    );

    if (isNotJson) {
      return next.handle().pipe(map((result: any) => result));
    }

    return next.handle().pipe(
      map((result: any) => {
        let data = result;

        const meta = new JsonMeta();
        const paging = new JsonPage();
        const json = new JsonResponse();

        if (!!result.data) {
          data = result.data;

          if (result?.query?.range?.length > 0) {
            const [currentPage, totalItemsPerPage] = result.query.range || [];

            paging.totalItems = result.count || 0;
            paging.currentPage = currentPage || 0;
            paging.itemsPerPage = totalItemsPerPage || 0;
            paging.totalPages =
              Math.round(result.count / totalItemsPerPage) || 0;
          }
        }

        meta.url = request.url;
        meta.method = request.method;
        meta.status = response.statusCode;
        meta.paging = paging;

        json.meta = meta;
        json.data = Array.isArray(data) ? data : [data];
        json.errors = null;

        return classToPlain(json);
      }),
    );
  }
}
