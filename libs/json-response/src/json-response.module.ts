import { Module } from '@nestjs/common';
import { JsonResponseService } from './json-response.service';

@Module({
  providers: [JsonResponseService],
  exports: [JsonResponseService],
})
export class JsonResponseModule {}
