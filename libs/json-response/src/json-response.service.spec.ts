import { Test, TestingModule } from '@nestjs/testing';
import { JsonResponseService } from './json-response.service';

describe('JsonResponseService', () => {
  let service: JsonResponseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JsonResponseService],
    }).compile();

    service = module.get<JsonResponseService>(JsonResponseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
