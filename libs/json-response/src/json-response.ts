import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';

export class JsonPage {
  @ApiProperty({ name: 'total_items' })
  @Expose({ name: 'total_items' })
  totalItems: number = null;

  @ApiProperty({ name: 'total_pages' })
  @Expose({ name: 'total_pages' })
  totalPages: number = null;

  @ApiProperty({ name: 'current_page' })
  @Expose({ name: 'current_page' })
  currentPage: number = null;

  @ApiProperty({ name: 'items_per_page' })
  @Expose({ name: 'items_per_page' })
  itemsPerPage: number = null;
}

export class JsonMeta {
  @ApiProperty({ name: 'url' })
  @Expose({ name: 'url' })
  url: string;

  @ApiProperty({ name: 'method' })
  @Expose({ name: 'method' })
  method: string;

  @ApiProperty({ name: 'status' })
  @Expose({ name: 'status' })
  status: number;

  @ApiProperty({ name: 'paging' })
  @Expose({ name: 'paging' })
  @Type(() => JsonPage)
  paging?: JsonPage;
}

export class JsonError {
  @ApiProperty({ name: 'code' })
  @Expose({ name: 'code' })
  code: string;

  @ApiProperty({ name: 'detail' })
  @Expose({ name: 'detail' })
  detail: string;
}

export class JsonResponse<T> {
  @ApiProperty({ name: 'meta' })
  @Expose({ name: 'meta' })
  @Type(() => JsonMeta)
  meta: JsonMeta;

  @ApiProperty({ name: 'data' })
  @Expose({ name: 'data' })
  data: T[];

  @ApiProperty({ name: 'errors' })
  @Expose({ name: 'errors' })
  @Type(() => JsonError)
  errors: JsonError[];
}
