import { IsArray, IsObject, IsOptional, IsString } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class QueryParamsDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  public q: string;

  @ApiPropertyOptional()
  @IsArray()
  @IsOptional()
  @Transform(({ value }) => {
    try {
      return JSON.parse(value || ('[]' as any));
    } catch (err) {
      return null;
    }
  })
  public sort: string[] = ['created_at', 'DESC'];

  @ApiPropertyOptional()
  @IsArray()
  @IsOptional()
  @Transform(({ value }) => {
    try {
      return JSON.parse(value || ('[]' as any));
    } catch (err) {
      return null;
    }
  })
  public range: number[] = [0, 10];

  @ApiPropertyOptional()
  @IsObject()
  @IsOptional()
  @Transform(({ value }) => {
    try {
      return JSON.parse(value || ('{}' as any));
    } catch (err) {
      return null;
    }
  })
  public filter: {
    [key: string]: any;
  };
}
