import { Module, Global } from '@nestjs/common';

import { QueryParserService } from './query-parser.service';

@Global()
@Module({
  providers: [QueryParserService],
  exports: [QueryParserService],
})
export class QueryParserModule {}
