import { Injectable } from '@nestjs/common';
import camelCase from 'lodash.camelcase';

@Injectable()
export class QueryParserService {
  public asPrismaQuery({ sort, filter, range }: Record<string, any>) {
    const query: any = {};

    if (sort) {
      if (Array.isArray(sort) && sort.length === 2) {
        const [key, value] = sort;

        query.orderBy = {
          [key]: value.toLowerCase(),
        };
      }
    }

    if (range) {
      if (Array.isArray(range) && range.length === 2) {
        const [start, end] = range;

        query.skip = start;
        query.take = end - start;
      }
    }

    if (filter) {
      if (Object.keys(filter).length > 0) {
        query.where = filter;
      }
    }

    return query;
  }

  public asTypeOrmQuery({ sort, filter, range }: Record<string, any>) {
    const query: any = {};

    if (sort) {
      if (Array.isArray(sort) && sort.length === 2) {
        const [key, value] = sort;

        query.order = {
          [camelCase(key)]: value.toUpperCase(),
        };
      }
    }

    if (range) {
      if (Array.isArray(range) && range.length === 2) {
        const [start, end] = range;

        query.skip = start;
        query.take = end - start;
      }
    }

    if (filter) {
      if (Object.keys(filter).length > 0) {
        query.where = filter;
      }
    }

    return query;
  }
}
