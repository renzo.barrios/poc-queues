import { getSchemaPath } from '@nestjs/swagger';
import { applyDecorators, Type } from '@nestjs/common';

export const ApiGenericResponse = <TModel extends Type<any>>(
  response: TModel,
  model: any,
  decorator: any,
) => {
  if (!decorator) {
    return null;
  }

  return applyDecorators(
    decorator({
      schema: {
        allOf: [
          {
            $ref: getSchemaPath(response),
          },
          {
            properties: {
              data: {
                type: 'array',
                items: {
                  $ref: getSchemaPath(model),
                },
              },
            },
          },
        ],
      },
    }),
  );
};
