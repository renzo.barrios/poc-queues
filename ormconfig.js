module.exports = {
  type: process.env.DATABASE_TYPE,
  autoLoadEntities: true,
  host: process.env.DATABASE_HOST,
  database: process.env.DATABASE_NAME,
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  logging: true,
  synchronize: true,
  options: {
    useUTC: true,
    trustedConnection: false,
    trustServerCertificate: true,
    port: process.env.DATABASE_PORT,
  },
  seeds: [process.env.TYPEORM_SEEDING_SEEDS],
  factories: [process.env.TYPEORM_SEEDING_FACTORIES],
  entities: [process.env.TYPEORM_SEEDING_ENTITIES],
};
