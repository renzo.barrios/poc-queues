const { promisify } = require('util');
const { exec } = require('child_process');

const asyncExec = promisify(exec);
const capitalizeFirstLetter = (string) =>
  `${string.charAt(0).toUpperCase()}${string.slice(1)}`;

// TODO: work as a CLI that checks software from a JSON obtained from some server, and installs required software on per-platform basis

const validateSetup = async () => {
  const requiredSoftware = ['npm', 'node', 'docker', 'docker-compose'];
  const requiredSoftwareResults = await Promise.all(
    requiredSoftware.map(async (software) => {
      const { stdout, stderr } = await asyncExec(`${software} --version`);

      return {
        version: stdout,
        error: stderr,
        software,
      };
    }),
  );

  requiredSoftwareResults.forEach(({ software, error, version }) => {
    const tag = `[${capitalizeFirstLetter(software)} Version]`;

    if (!!version) {
      console.info(`${tag}: ${version}`);
    }

    if (!!error) {
      console.error(`${tag}: Unable to obtain version with error ${error}`);
    }
  });
};

validateSetup();
