import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import {
  ClassSerializerInterceptor,
  Logger,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';

import {
  JsonResponseInterceptor,
  JsonResponseExceptionFilter,
} from '@api-c3/json-response';
import { QueryParserModule } from '@api-c3/query-parser';

import { AuthModule } from './auth/auth.module';
import { UserModule } from './users/user.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { BullModule } from '@nestjs/bull';
import { CalculationModule } from './calculations/calculation.module';
import { CalculationProducerService } from './calculations/calculation.producer.service';
import { ReportModule } from './reports/report.module';

@Module({
  imports: [
    QueryParserModule,
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      expandVariables: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: configService.get<any>('DATABASE_TYPE'),
        autoLoadEntities: true,
        host: configService.get<string>('DATABASE_HOST'),
        port: configService.get<number>('DATABASE_PORT'),
        database: configService.get<string>('DATABASE_NAME'),
        username: configService.get<string>('DATABASE_USER'),
        password: configService.get<string>('DATABASE_PASSWORD'),
        logging: configService.get<string>('APP_ENV') !== 'production',
        synchronize: configService.get<string>('APP_ENV') !== 'production',
        options: {
          useUTC: true,
          trustedConnection: false,
          trustServerCertificate: true,
          port: configService.get<number>('DATABASE_PORT'),
        },
      }),
      inject: [ConfigService],
    }),
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    AuthModule,
    UserModule,
    CalculationModule,
    ReportModule,
  ],
  providers: [
    Logger,
    /*{
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },*/
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: JsonResponseInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: JsonResponseExceptionFilter,
    },
    //CalculationProducerService,
    //{
    //provide: APP_PIPE,
    //useFactory: () =>
    //new ValidationPipe({
    //whitelist: true,
    //transform: true,
    //forbidNonWhitelisted: false,
    //transformOptions: {
    //enableImplicitConversion: true,
    //},
    //}),
    //},
  ],
})
export class AppModule {}
