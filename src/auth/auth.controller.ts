import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  Controller,
  Get,
  HttpCode,
  Inject,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';

import { LoginDto } from './dto/login.dto';

import { Public } from './auth.decorator';
import { AuthService } from './auth.service';

@ApiTags('Auth')
@Controller('auth')
@ApiExtraModels(LoginDto)
export class AuthController {
  @Inject()
  private readonly authService: AuthService;

  @ApiOperation({
    summary: 'Login',
    description: 'Generate a new user session',
  })
  @Public()
  @UseGuards(AuthGuard('local'))
  @HttpCode(200)
  @Post('login')
  public async login(@Req() req): Promise<LoginDto> {
    return await this.authService.login(req.user);
  }

  @ApiOperation({
    summary: 'Refresh Token',
    description: 'Refresh a user session',
  })
  @ApiBearerAuth()
  @HttpCode(200)
  @Post('refresh-token')
  public async refreshToken(@Req() req): Promise<LoginDto> {
    return await this.authService.refreshToken(req.user);
  }

  @ApiOperation({
    summary: 'Logout',
    description: 'Remove the user session',
  })
  @ApiBearerAuth()
  @Get('logout')
  public async logout(@Req() req): Promise<any> {
    return await this.authService.logout(req.user);
  }

  @ApiOperation({
    summary: 'User Profile',
    description: 'Gets the user profile information',
  })
  @ApiBearerAuth()
  @Get('me')
  public async me(@Req() req): Promise<any> {
    return req.user;
  }
}
