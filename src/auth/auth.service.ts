import {
  CacheStore,
  CACHE_MANAGER,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { plainToClass } from 'class-transformer';

import { LoginDto } from './dto/login.dto';
import { UseLogger } from '@api-c3/aop';

@Injectable()
export class AuthService {
  @Inject(CACHE_MANAGER)
  private readonly cacheStore: CacheStore;

  @Inject()
  private readonly jwtService: JwtService;

  @Inject()
  private readonly configService: ConfigService;

  @UseLogger()
  public async login(user: any): Promise<LoginDto> {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        { data: user },
        {
          secret: this.configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
          expiresIn: this.configService.get<string>(
            'JWT_ACCESS_TOKEN_EXPIRES_IN_SECONDS',
          ),
        },
      ),
      this.jwtService.signAsync(
        {
          data: {
            id: user.id,
          },
        },
        {
          secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
          expiresIn: this.configService.get<string>(
            'JWT_REFRESH_TOKEN_EXPIRES_IN_SECONDS',
          ),
        },
      ),
    ]);

    const appEnv = this.configService.get<string>('APP_ENV');

    await Promise.all([
      this.cacheStore.set(
        `${appEnv}/users/${user.id}/access-token`,
        accessToken,
        {
          ttl: this.configService.get<number>(
            'JWT_ACCESS_TOKEN_EXPIRES_IN_SECONDS',
          ),
        },
      ),
      this.cacheStore.set(
        `${appEnv}/users/${user.id}/refresh-token`,
        refreshToken,
        {
          ttl: this.configService.get<number>(
            'JWT_REFRESH_TOKEN_EXPIRES_IN_SECONDS',
          ),
        },
      ),
    ]);

    return plainToClass(LoginDto, {
      access_token: accessToken,
      refresh_token: refreshToken,
    });
  }

  @UseLogger()
  public async refreshToken(user: any): Promise<LoginDto> {
    const appEnv = this.configService.get<string>('APP_ENV');

    const refreshToken = await this.cacheStore.get(
      `${appEnv}/users/${user.id}/refresh-token`,
    );

    if (!refreshToken) {
      throw new UnauthorizedException('Refresh Token has expired');
    }

    await this.logout(user);

    return await this.login(user);
  }

  @UseLogger()
  public async logout(user: any): Promise<any> {
    const appEnv = this.configService.get<string>('APP_ENV');

    await Promise.all([
      this.cacheStore.del(`${appEnv}/users/${user.id}/access-token`),
      this.cacheStore.del(`${appEnv}/users/${user.id}/refresh-token`),
    ]);

    return {
      message: 'User successfully logged out',
    };
  }
}
