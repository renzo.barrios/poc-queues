import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class LoginDto {
  @ApiProperty({
    name: 'access_token',
  })
  public accessToken: string;

  @ApiProperty({
    name: 'refresh_token',
  })
  public refreshToken: string;
}
