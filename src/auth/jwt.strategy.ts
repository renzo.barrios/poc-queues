import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import {
  Inject,
  Injectable,
  CacheStore,
  CACHE_MANAGER,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { UseLogger } from '@api-c3/aop';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  @Inject(CACHE_MANAGER)
  private readonly cacheStore: CacheStore;

  @Inject()
  private readonly configService: ConfigService;

  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_ACCESS_TOKEN_SECRET,
      ignoreExpiration: true,
    });
  }

  @UseLogger()
  public async validate(jwtPayload: any) {
    const appEnv = this.configService.get('APP_ENV');

    const accessToken = await this.cacheStore.get(
      `${appEnv}/users/${jwtPayload.data.id}/access-token`,
    );

    if (!accessToken) {
      throw new UnauthorizedException(`Access Token has expired`);
    }

    return jwtPayload.data;
  }
}
