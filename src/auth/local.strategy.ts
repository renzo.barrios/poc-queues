import { Repository } from 'typeorm';
import { Strategy } from 'passport-local';
import { InjectRepository } from '@nestjs/typeorm';
import { PassportStrategy } from '@nestjs/passport';
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UseLogger, UseThrowException } from '@api-c3/aop';
import { ConfigService } from '@nestjs/config';

import { User } from '../users/entity/user.entity';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  @InjectRepository(User)
  private readonly userRepository: Repository<User>;

  @Inject()
  private readonly configService: ConfigService;

  @UseLogger()
  @UseThrowException()
  public async validate(
    username: string,
    password: string,
  ): Promise<Partial<User>> {
    const user = await this.userRepository.findOne({
      where: {
        username,
      },
    });

    if (!user) {
      throw new UnauthorizedException();
    }

    const saltRounds = this.configService.get('SALT_OR_ROUNDS');

    const hash = await bcrypt.hash(password, +saltRounds);

    const isMatch = await bcrypt.compare(password, hash);
    if (!isMatch) {
      throw new UnauthorizedException('User not found');
    }

    // Must be an plain object
    return {
      ...user,
    };
  }
}
