import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';

/*each queue must have its own consumer*/
@Processor('calculation-queue')
export class CalculationConsumer {
  /*job handler*/

  /*this name was setted in the producer*/
  @Process('calculation-job')
  calculationJob(job: Job<unknown>) {
    console.log('SECOND >>>>>>>>>>>>>>>>>>>>>> Handle the job', job.data);
  }
}
