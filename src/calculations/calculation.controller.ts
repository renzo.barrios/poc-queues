import {
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Inject,
  Controller,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiBearerAuth,
  ApiExtraModels,
  ApiOkResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';

import { ApiGenericResponse } from '@api-c3/swagger';
import { JsonResponse } from '@api-c3/json-response';
import { QueryParamsDto } from '@api-c3/query-parser';

import { Calculation } from './entity/calculation.entity';
import { CreateCalculationDto } from './dto/create-calculation.dto';
import { UpdateCalculationDto } from './dto/update-calculation.dto';

import { CalculationService } from './calculation.service';

@ApiTags('Calculations')
@Controller('calculations')
@ApiExtraModels(JsonResponse, Calculation)
export class CalculationController {
  @Inject()
  private readonly calculationService: CalculationService;

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Create',
    description: 'Create a new Calculation',
  })
  @ApiGenericResponse(JsonResponse, Calculation, ApiCreatedResponse)
  @Get('add-calculation')
  public async create(@Query('calc') calc: number): Promise<Calculation> {
    console.log('CONTROLLER', calc);
    return await this.calculationService.create(calc);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get All',
    description: 'Gets all Calculations',
  })
  @ApiGenericResponse(JsonResponse, Calculation, ApiOkResponse)
  @Get()
  public async findAll(@Query() query: QueryParamsDto): Promise<{
    data: Calculation[];
    count: number;
    query: QueryParamsDto;
  }> {
    const [data, count] = await Promise.all([
      this.calculationService.findAll(query),
      this.calculationService.count(),
    ]);

    return {
      data,
      count,
      query,
    };
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get by ID',
    description: 'Get a Calculation by ID',
  })
  @ApiGenericResponse(JsonResponse, Calculation, ApiOkResponse)
  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Calculation> {
    return await this.calculationService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Update by ID',
    description: 'Update a Calculation by ID',
  })
  @ApiGenericResponse(JsonResponse, Calculation, ApiOkResponse)
  @Put(':id')
  public async update(
    @Param('id') id: string,
    @Body() updateDto: UpdateCalculationDto,
  ): Promise<Calculation> {
    return await this.calculationService.update(id, updateDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete by ID',
    description: 'Delete a Calculation by ID',
  })
  @ApiGenericResponse(JsonResponse, Calculation, ApiOkResponse)
  @Delete(':id')
  public async delete(@Param('id') id: string): Promise<Calculation> {
    return await this.calculationService.delete(id);
  }
}
