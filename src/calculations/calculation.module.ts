import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CalculationConsumer } from './calculation.consumer';

import { CalculationController } from './calculation.controller';
import { CalculationProducerService } from './calculation.producer.service';
import { CalculationService } from './calculation.service';

import { Calculation } from './entity/calculation.entity';

@Module({
  imports: [
    BullModule.registerQueue({
      /*name that need to be asociated with the producer*/
      name: 'calculation-queue',
    }),
    TypeOrmModule.forFeature([Calculation]),
  ],
  controllers: [CalculationController],
  providers: [
    CalculationService,
    CalculationProducerService,
    CalculationConsumer,
  ],
  exports: [TypeOrmModule],
})
export class CalculationModule {}
