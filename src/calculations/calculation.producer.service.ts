import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
const fs = require('fs');
import { Queue } from 'bull';

@Injectable()
export class CalculationProducerService {
  constructor(@InjectQueue('calculation-queue') private queue: Queue) {}

  public async sendCalculation(calc: string) {
    console.log(' FIRST >>>>>>>>>>>>>>>>>>>>>>>>  CREATING THE JOB', calc);
    // logic to calculate a expesive calculation

    /*This name must be handle by the consumer*/
    await this.queue.add(
      'calculation-job',
      {
        result: calc,
      },
      { delay: 40000 },
    );
  }
}
