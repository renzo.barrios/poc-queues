import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { UseLogger, UseThrowException } from '@api-c3/aop';
import { QueryParamsDto, QueryParserService } from '@api-c3/query-parser';

import { Calculation } from './entity/calculation.entity';

import { CreateCalculationDto } from './dto/create-calculation.dto';
import { UpdateCalculationDto } from './dto/update-calculation.dto';
import { CalculationProducerService } from './calculation.producer.service';

@Injectable()
export class CalculationService {
  @Inject()
  private readonly queryParser: QueryParserService;

  @Inject()
  private readonly calculationProducerService: CalculationProducerService;

  @InjectRepository(Calculation)
  private readonly calculationRepository: Repository<Calculation>;

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async count(): Promise<number> {
    return await this.calculationRepository.count();
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async create(createDto: any): Promise<any> {
    await this.calculationProducerService.sendCalculation(createDto);

    await this.calculationRepository.save(new Calculation(createDto));

    return 'Your very expensive calculation will be sent in the next hours to your email address';
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async findAll(query: QueryParamsDto): Promise<Calculation[]> {
    const parsedQuery = this.queryParser.asTypeOrmQuery(query);

    return await this.calculationRepository.find(parsedQuery);
  }

  @UseLogger()
  @UseThrowException(NotFoundException)
  public async findOne(id: string): Promise<Calculation> {
    return await this.calculationRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async update(
    id: string,
    updateDto: UpdateCalculationDto,
  ): Promise<Calculation> {
    await this.calculationRepository.update(id, updateDto);
    return await this.calculationRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async delete(id: string): Promise<Calculation> {
    const calculation = await this.calculationRepository.findOne(id);

    if (!!calculation) {
      await this.calculationRepository.delete(id);
    }

    return calculation;
  }
}
