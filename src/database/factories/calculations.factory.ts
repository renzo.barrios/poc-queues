import { define } from 'typeorm-seeding';
import * as Faker from 'faker';
import { Calculation } from '../../calculations/entity/calculation.entity';

define(
  Calculation,
  (
    faker: typeof Faker,
    context: {
      calc: number;
    },
  ) =>
    new Calculation({
      id: faker.random.uuid(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
      calc: context.calc,
    }),
);
