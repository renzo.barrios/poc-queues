import { define } from 'typeorm-seeding';
import * as Faker from 'faker';

import { Role } from '../../roles/entity/role.entity';

define(
  Role,
  (
    faker: typeof Faker,
    context: {
      name: string;
    },
  ) =>
    new Role({
      id: faker.random.uuid(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
      name: context.name,
    }),
);
