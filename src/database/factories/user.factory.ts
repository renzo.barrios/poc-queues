import { define } from 'typeorm-seeding';
import * as Faker from 'faker';
import * as bcrypt from 'bcrypt';

import { User } from '../../users/entity/user.entity';
import { Role } from '../../roles/entity/role.entity';

define(
  User,
  (
    faker: typeof Faker,
    context: {
      username: string;
      password: string;
      role: Role;
    },
  ) =>
    new User({
      id: faker.random.uuid(),
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
      role: context.role,
      username: context.username,
      password: bcrypt.hash(context.password, +process.env.SALT_OR_ROUNDS),
    }),
);
