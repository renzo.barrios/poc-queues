import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { Calculation } from '../../calculations/entity/calculation.entity';

import { Role } from '../../roles/entity/role.entity';
import { User } from '../../users/entity/user.entity';

// TODO: replace by real roles
const roles = [
  {
    name: 'SuperAdmin',
  },
];

const calculations = [
  {
    calc: 5,
  },
];

const users = [
  {
    username: 'admin@my-piroska.com',
    password: 'admin1234',
  },
  {
    username: 'foo.bar@my-piroska.com',
    password: 'foo1234',
  },
];

export default class CreateAdministrator implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<void> {
    const userRepository = connection.getRepository(User);
    const currentUsers = await userRepository.find();
    await userRepository.remove(currentUsers);

    const roleRepository = connection.getRepository(Role);
    const currentRoles = await roleRepository.find();
    await roleRepository.remove(currentRoles);

    const createdRoles = await Promise.all(
      roles.map((role) => factory(Role)(role).create()),
    );

    const calculationRepository = connection.getRepository(Calculation);
    const currentCals = await calculationRepository.find();
    await calculationRepository.remove(currentCals);

    await Promise.all(
      calculations.map((calc) => factory(Calculation)(calc).create()),
    );

    await Promise.all(
      users.map((user, index) =>
        factory(User)({
          ...user,
          role: createdRoles[index],
        }).create(),
      ),
    );
  }
}
