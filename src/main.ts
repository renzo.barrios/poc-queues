import helmet from 'helmet';

import { RedocModule } from 'nestjs-redoc';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';

import { AppModule } from './app.module';

// TODO: API Key
// TODO: Permissions API
// TODO: Auth login, refresh-token, logout

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: true,
  });

  const configService: ConfigService = app.get(ConfigService);

  const isSwaggerEnabled = configService.get<boolean>('DOCUMENTATION_SHOW');
  const port = configService.get<number>('APP_PORT');

  if (isSwaggerEnabled) {
    const uri = configService.get<string>('DOCUMENTATION_URL');
    const title = configService.get<string>('DOCUMENTATION_TITLE');
    const version = configService.get<string>('DOCUMENTATION_VERSION');
    const description = configService.get<string>('DOCUMENTATION_DESCRIPTION');

    const document = SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .setTitle(title)
        .setDescription(description)
        .setVersion(version)
        .addBearerAuth()
        .build(),
    );

    await RedocModule.setup(uri, app as any, document, {
      sortPropsAlphabetically: true,
      pathInMiddlePanel: false,
      hideDownloadButton: true,
      disableSearch: true,
      hideHostname: true,
      noAutoAuth: true,
      title,
    });
  }

  app.disable('x-powered-by');
  app.use(helmet());

  await app.listen(port);
}

bootstrap();
