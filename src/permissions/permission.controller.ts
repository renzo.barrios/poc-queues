import {
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Inject,
  Controller,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiBearerAuth,
  ApiExtraModels,
  ApiOkResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';

import { ApiGenericResponse } from '@api-c3/swagger';
import { JsonResponse } from '@api-c3/json-response';
import { QueryParamsDto } from '@api-c3/query-parser';

import { Permission } from './entity/permission.entity';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';

import { PermissionService } from './permission.service';

@ApiTags('Permissions')
@Controller('permissions')
@ApiExtraModels(JsonResponse, Permission)
export class PermissionController {
  @Inject()
  private readonly permissionService: PermissionService;

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Create',
    description: 'Create a new Permission',
  })
  @ApiGenericResponse(JsonResponse, Permission, ApiCreatedResponse)
  @Post()
  public async create(@Body() createDto: CreatePermissionDto): Promise<Permission> {
    return await this.permissionService.create(createDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get All',
    description: 'Gets all Permissions',
  })
  @ApiGenericResponse(JsonResponse, Permission, ApiOkResponse)
  @Get()
    public async findAll(
    @Query() query: QueryParamsDto,
  ): Promise<{
    data: Permission[];
    count: number;
    query: QueryParamsDto;
  }> {
    const [data, count] = await Promise.all([
      this.permissionService.findAll(query),
      this.permissionService.count(),
    ]);

    return {
      data,
      count,
      query,
    };
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get by ID',
    description: 'Get a Permission by ID',
  })
  @ApiGenericResponse(JsonResponse, Permission, ApiOkResponse)
  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Permission> {
    return await this.permissionService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Update by ID',
    description: 'Update a Permission by ID',
  })
  @ApiGenericResponse(JsonResponse, Permission, ApiOkResponse)
  @Put(':id')
  public async update(
    @Param('id') id: string,
    @Body() updateDto: UpdatePermissionDto,
  ): Promise<Permission> {
    return await this.permissionService.update(id, updateDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete by ID',
    description: 'Delete a Permission by ID',
  })
  @ApiGenericResponse(JsonResponse, Permission, ApiOkResponse)
  @Delete(':id')
  public async delete(@Param('id') id: string): Promise<Permission> {
    return await this.permissionService.delete(id);
  }
}
