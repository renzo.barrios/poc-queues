import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { UseLogger, UseThrowException } from '@api-c3/aop';
import { QueryParamsDto, QueryParserService } from '@api-c3/query-parser';

import { Permission } from './entity/permission.entity';

import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';


@Injectable()
export class PermissionService {
  @Inject()
  private readonly queryParser: QueryParserService;

  @InjectRepository(Permission)
  private readonly permissionRepository: Repository<Permission>;

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async count(): Promise<number> {
    return await this.permissionRepository.count();
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async create(createDto: CreatePermissionDto): Promise<Permission> {
    return await this.permissionRepository.save(new Permission(createDto));
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async findAll(query: QueryParamsDto): Promise<Permission[]> {
    const parsedQuery = this.queryParser.asTypeOrmQuery(query);

    return await this.permissionRepository.find(parsedQuery);
  }

  @UseLogger()
  @UseThrowException(NotFoundException)
  public async findOne(id: string): Promise<Permission> {
    return await this.permissionRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async update(id: string, updateDto: UpdatePermissionDto): Promise<Permission> {
    await this.permissionRepository.update(id, updateDto);
    return await this.permissionRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async delete(id: string): Promise<Permission> {
    const [permission] = await Promise.all([
      this.permissionRepository.findOne(id),
      this.permissionRepository.delete(id),
    ]);

    return permission;
  }
}