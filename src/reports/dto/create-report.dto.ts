export class CreateReportDto {
  reportBy: string;

  code: number;

  result: string;

  status: string;
}
