import { PartialType } from '@nestjs/mapped-types';

import { CreateReportDto } from './create-report.dto';

export class UpdateReportDto extends PartialType(CreateReportDto) {
  reportBy: string;

  code: number;

  result: string;

  status: string;
}
