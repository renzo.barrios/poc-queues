import { ApiProperty } from '@nestjs/swagger';
import {
  Entity,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  Column,
} from 'typeorm';

@Entity('reports')
export class Report {
  @ApiProperty({
    name: 'id',
  })
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @ApiProperty({
    name: 'created_at',
  })
  @CreateDateColumn({
    name: 'created_at',
  })
  public createdAt: Date;

  @Column({
    name: 'reporterBy',
  })
  public reporterBy: string;

  @ApiProperty({
    name: 'updated_at',
  })
  @UpdateDateColumn({
    name: 'updated_at',
  })
  public updatedAt: Date;

  constructor(data: any) {
    Object.assign(this, data);
  }
}
