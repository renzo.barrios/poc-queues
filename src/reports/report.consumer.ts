import { OnQueueActive, Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import path from 'path';
import fs from 'fs/promises';
import { CreateReportDto } from './dto/create-report.dto';

@Processor('report-queue')
export class ReportConsumer {
  @Process('report-job')
  async readOperationJob(job: Job<CreateReportDto>) {
    const pathRead = path.resolve(__dirname, `../reports`);

    const pathCreate = path.resolve(
      __dirname,
      `../reports/${job.data?.reportBy ?? 'report'}.txt`,
    );

    // SIMULATE JOB FAILED
    if (job.data.reportBy === 'sarasa') {
      console.log('Intento >>>>>', job.attemptsMade);

      await job.moveToFailed({
        message: 'Ah enviado en titulo sarasa',
      });

      await job.discard();
      return;
    }

    await fs.writeFile(
      pathCreate,
      JSON.stringify({
        ...job.data,
        intentos: job.attemptsMade,
      }),
    );

    //file written successfully
    console.log('file written successfully');
  }

  @OnQueueActive()
  onActive(job: Job) {
    console.log(
      `Processing job ${job.id} of type ${job.name} with data ${job.data}...`,
    );
  }
}
