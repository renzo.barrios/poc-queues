import {
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Inject,
  Controller,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiBearerAuth,
  ApiExtraModels,
  ApiOkResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';

import { ApiGenericResponse } from '@api-c3/swagger';
import { JsonResponse } from '@api-c3/json-response';
import { QueryParamsDto } from '@api-c3/query-parser';

import { Report } from './entity/report.entity';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';

import { ReportService } from './report.service';

@ApiTags('Reports')
@Controller('reports')
@ApiExtraModels(JsonResponse, Report)
export class ReportController {
  @Inject()
  private readonly reportService: ReportService;

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Create',
    description: 'Create a new Report',
  })
  //@ApiGenericResponse(JsonResponse, Report, ApiCreatedResponse)
  @Post()
  public async create(@Body() createDto: CreateReportDto): Promise<Report> {
    console.log('REPORT CONTROLLER', createDto);
    return await this.reportService.create(createDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get All',
    description: 'Gets all Reports',
  })
  @ApiGenericResponse(JsonResponse, Report, ApiOkResponse)
  @Get()
  public async findAll(@Query() query: QueryParamsDto): Promise<{
    data: Report[];
    count: number;
    query: QueryParamsDto;
  }> {
    const [data, count] = await Promise.all([
      this.reportService.findAll(query),
      this.reportService.count(),
    ]);

    return {
      data,
      count,
      query,
    };
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get by ID',
    description: 'Get a Report by ID',
  })
  @ApiGenericResponse(JsonResponse, Report, ApiOkResponse)
  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Report> {
    return await this.reportService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Update by ID',
    description: 'Update a Report by ID',
  })
  @ApiGenericResponse(JsonResponse, Report, ApiOkResponse)
  @Put(':id')
  public async update(
    @Param('id') id: string,
    @Body() updateDto: UpdateReportDto,
  ): Promise<Report> {
    return await this.reportService.update(id, updateDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete by ID',
    description: 'Delete a Report by ID',
  })
  @ApiGenericResponse(JsonResponse, Report, ApiOkResponse)
  @Delete(':id')
  public async delete(@Param('id') id: string): Promise<Report> {
    return await this.reportService.delete(id);
  }
}
