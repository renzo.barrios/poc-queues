import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ReportController } from './report.controller';
import { ReportService } from './report.service';

import { Report } from './entity/report.entity';
import { BullModule } from '@nestjs/bull';
import { ReportConsumer } from './report.consumer';
import { ReportProducerService } from './report.producer.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Report]),

    BullModule.registerQueue({
      /*name that need to be asociated with the producer*/
      name: 'report-queue',
    }),
  ],
  controllers: [ReportController],
  providers: [ReportService, ReportConsumer, ReportProducerService],
  exports: [TypeOrmModule],
})
export class ReportModule {}

