import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import { CreateReportDto } from './dto/create-report.dto';

@Injectable()
export class ReportProducerService {
  constructor(@InjectQueue('report-queue') private queue: Queue) {}

  async sendReport(report: CreateReportDto) {
    console.log('PRODUCE JOB', report);
    await this.queue.add('report-job', report, { delay: 10000, attempts: 5 });
  }
}
