import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { UseLogger, UseThrowException } from '@api-c3/aop';
import { QueryParamsDto, QueryParserService } from '@api-c3/query-parser';

import { Report } from './entity/report.entity';

import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { ReportProducerService } from './report.producer.service';

@Injectable()
export class ReportService {
  @Inject()
  private readonly queryParser: QueryParserService;

  @Inject()
  private readonly reportProducerService: ReportProducerService;

  @InjectRepository(Report)
  private readonly reportRepository: Repository<Report>;

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async count(): Promise<number> {
    return await this.reportRepository.count();
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async create(createDto: CreateReportDto): Promise<any> {
    await this.reportProducerService.sendReport(createDto);

    return 'Your report will be ready in 4hs';
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async findAll(query: QueryParamsDto): Promise<Report[]> {
    const parsedQuery = this.queryParser.asTypeOrmQuery(query);

    return await this.reportRepository.find(parsedQuery);
  }

  @UseLogger()
  @UseThrowException(NotFoundException)
  public async findOne(id: string): Promise<Report> {
    return await this.reportRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async update(id: string, updateDto: UpdateReportDto): Promise<Report> {
    await this.reportRepository.update(id, updateDto);
    return await this.reportRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async delete(id: string): Promise<Report> {
    const report = await this.reportRepository.findOne(id);

    if (!!report) {
      await this.reportRepository.delete(id);
    }

    return report;
  }
}

