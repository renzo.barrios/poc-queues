import {
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Inject,
  Controller,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiBearerAuth,
  ApiExtraModels,
  ApiOkResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';

import { ApiGenericResponse } from '@api-c3/swagger';
import { JsonResponse } from '@api-c3/json-response';
import { QueryParamsDto } from '@api-c3/query-parser';

import { Role } from './entity/role.entity';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';

import { RoleService } from './role.service';

@ApiTags('Roles')
@Controller('roles')
@ApiExtraModels(JsonResponse, Role)
export class RoleController {
  @Inject()
  private readonly roleService: RoleService;

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Create',
    description: 'Create a new Role',
  })
  @ApiGenericResponse(JsonResponse, Role, ApiCreatedResponse)
  @Post()
  public async create(@Body() createDto: CreateRoleDto): Promise<Role> {
    return await this.roleService.create(createDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get All',
    description: 'Gets all Roles',
  })
  @ApiGenericResponse(JsonResponse, Role, ApiOkResponse)
  @Get()
    public async findAll(
    @Query() query: QueryParamsDto,
  ): Promise<{
    data: Role[];
    count: number;
    query: QueryParamsDto;
  }> {
    const [data, count] = await Promise.all([
      this.roleService.findAll(query),
      this.roleService.count(),
    ]);

    return {
      data,
      count,
      query,
    };
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get by ID',
    description: 'Get a Role by ID',
  })
  @ApiGenericResponse(JsonResponse, Role, ApiOkResponse)
  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Role> {
    return await this.roleService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Update by ID',
    description: 'Update a Role by ID',
  })
  @ApiGenericResponse(JsonResponse, Role, ApiOkResponse)
  @Put(':id')
  public async update(
    @Param('id') id: string,
    @Body() updateDto: UpdateRoleDto,
  ): Promise<Role> {
    return await this.roleService.update(id, updateDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete by ID',
    description: 'Delete a Role by ID',
  })
  @ApiGenericResponse(JsonResponse, Role, ApiOkResponse)
  @Delete(':id')
  public async delete(@Param('id') id: string): Promise<Role> {
    return await this.roleService.delete(id);
  }
}
