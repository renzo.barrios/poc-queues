import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { UseLogger, UseThrowException } from '@api-c3/aop';
import { QueryParamsDto, QueryParserService } from '@api-c3/query-parser';

import { Role } from './entity/role.entity';

import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';

@Injectable()
export class RoleService {
  @Inject()
  private readonly queryParser: QueryParserService;

  @InjectRepository(Role)
  private readonly roleRepository: Repository<Role>;

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async count(): Promise<number> {
    return await this.roleRepository.count();
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async create(createDto: CreateRoleDto): Promise<Role> {
    return await this.roleRepository.save(new Role(createDto));
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async findAll(query: QueryParamsDto): Promise<Role[]> {
    const parsedQuery = this.queryParser.asTypeOrmQuery(query);

    return await this.roleRepository.find(parsedQuery);
  }

  @UseLogger()
  @UseThrowException(NotFoundException)
  public async findOne(id: string): Promise<Role> {
    return await this.roleRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async update(id: string, updateDto: UpdateRoleDto): Promise<Role> {
    await this.roleRepository.update(id, updateDto);
    return await this.roleRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async delete(id: string): Promise<Role> {
    const role = await this.roleRepository.findOne(id);

    if (!!role) {
      await this.roleRepository.delete(id);
    }

    return role;
  }
}
