import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    name: 'username',
  })
  @IsString()
  public username: string;

  @ApiProperty({
    name: 'password',
  })
  @IsString()
  public password: string;
}
