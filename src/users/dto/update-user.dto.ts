import { PartialType } from '@nestjs/mapped-types';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @ApiPropertyOptional({
    name: 'username',
  })
  @IsString()
  @IsOptional()
  public username: string;

  @ApiPropertyOptional({
    name: 'password',
  })
  @IsString()
  @IsOptional()
  public password: string;
}
