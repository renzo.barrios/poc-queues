import { ApiProperty } from '@nestjs/swagger';
import { Expose, Exclude } from 'class-transformer';
import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('users')
export class User {
  @ApiProperty({
    name: 'id',
  })
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @ApiProperty({
    name: 'username',
  })
  @Column('varchar', {
    name: 'username',
  })
  public username: string;

  @ApiProperty({
    name: 'password',
  })
  @Column('varchar', {
    name: 'password',
  })
  @Exclude()
  public password: string;

  @ApiProperty({
    name: 'created_at',
  })
  @Expose({
    name: 'created_at',
  })
  @CreateDateColumn({
    name: 'created_at',
  })
  public createdAt: Date;

  @ApiProperty({
    name: 'updated_at',
  })
  @Expose({
    name: 'updated_at',
  })
  @UpdateDateColumn({
    name: 'updated_at',
  })
  public updatedAt: Date;

  constructor(user: any) {
    Object.assign(this, user);
  }
}
