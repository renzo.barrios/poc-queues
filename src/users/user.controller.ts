import {
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
  Inject,
  Controller,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiBearerAuth,
  ApiExtraModels,
  ApiOkResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';

import { ApiGenericResponse } from '@api-c3/swagger';
import { JsonResponse } from '@api-c3/json-response';
import { QueryParamsDto } from '@api-c3/query-parser';

import { User } from './entity/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

import { UserService } from './user.service';

@ApiTags('Users')
@Controller('users')
@ApiExtraModels(JsonResponse, User)
export class UserController {
  @Inject()
  private readonly userService: UserService;

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Create',
    description: 'Create a new User',
  })
  @ApiGenericResponse(JsonResponse, User, ApiCreatedResponse)
  @Post()
  public async create(@Body() createDto: CreateUserDto): Promise<User> {
    return await this.userService.create(createDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get All',
    description: 'Gets all Users',
  })
  @ApiGenericResponse(JsonResponse, User, ApiOkResponse)
  @Get()
  public async findAll(@Query() query: QueryParamsDto): Promise<{
    data: User[];
    count: number;
    query: QueryParamsDto;
  }> {
    const [data, count] = await Promise.all([
      this.userService.findAll(query),
      this.userService.count(),
    ]);

    return {
      data,
      count,
      query,
    };
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get by ID',
    description: 'Get a User by ID',
  })
  @ApiGenericResponse(JsonResponse, User, ApiOkResponse)
  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<User> {
    return await this.userService.findOne(id);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Update by ID',
    description: 'Update a User by ID',
  })
  @ApiGenericResponse(JsonResponse, User, ApiOkResponse)
  @Put(':id')
  public async update(
    @Param('id') id: string,
    @Body() updateDto: UpdateUserDto,
  ): Promise<User> {
    return await this.userService.update(id, updateDto);
  }

  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete by ID',
    description: 'Delete a User by ID',
  })
  @ApiGenericResponse(JsonResponse, User, ApiOkResponse)
  @Delete(':id')
  public async delete(@Param('id') id: string): Promise<User> {
    return await this.userService.delete(id);
  }
}
