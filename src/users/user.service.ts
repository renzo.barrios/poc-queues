import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import { UseLogger, UseThrowException } from '@api-c3/aop';
import { QueryParamsDto, QueryParserService } from '@api-c3/query-parser';

import { User } from './entity/user.entity';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UserService {
  @Inject()
  private readonly queryParser: QueryParserService;

  @InjectRepository(User)
  private readonly userRepository: Repository<User>;

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async count(): Promise<number> {
    return await this.userRepository.count();
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async create(createDto: CreateUserDto): Promise<User> {
    return await this.userRepository.save(new User(createDto));
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async findAll(query: QueryParamsDto): Promise<User[]> {
    const parsedQuery = this.queryParser.asTypeOrmQuery(query);

    return await this.userRepository.find(parsedQuery);
  }

  @UseLogger()
  @UseThrowException(NotFoundException)
  public async findOne(id: string): Promise<User> {
    return await this.userRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(UnprocessableEntityException)
  public async update(id: string, updateDto: UpdateUserDto): Promise<User> {
    await this.userRepository.update(id, updateDto);
    return await this.userRepository.findOne(id);
  }

  @UseLogger()
  @UseThrowException(InternalServerErrorException)
  public async delete(id: string): Promise<User> {
    const user = await this.userRepository.findOne(id);

    if (!!user) {
      await this.userRepository.delete(id);
    }

    return user;
  }
}
